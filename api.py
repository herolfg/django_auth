from django.conf.urls import url, include

from viewsets import LogoutViewset, LoginViewset, SignupViewset, ResetViewset, ChangePasswordViewset, UserViewset
from rest_framework import routers

router = routers.DefaultRouter()
router.register(r'user', UserViewset, base_name="auth_user")

urlpatterns = [
    url(r'', include(router.urls)),
    url(r'^logout/$', LogoutViewset.as_view()),
    url(r'^login/$', LoginViewset.as_view()),
    url(r'^signup/$', SignupViewset.as_view()),
    url(r'^reset/$', ResetViewset.as_view()),
    url(r'^reset/.*$', ResetViewset.as_view()),
    url(r'^change-password/$', ChangePasswordViewset.as_view()),
]
