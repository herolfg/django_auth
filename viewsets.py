import datetime

from django.contrib.auth import authenticate, login, logout

from django.db.models import Q
from django.http import Http404
from django.core.mail import send_mail
from django.conf import settings

from rest_framework.permissions import AllowAny, IsAdminUser
from rest_framework.viewsets import ViewSet
from rest_framework.views import APIView
from rest_framework.response import Response
from rest_framework import status, views

from rest_framework_tracking.mixins import LoggingMixin

from django.contrib.auth import get_user_model
User = get_user_model()
from rest_framework import serializers
class UserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        exclude = ('password', )

def send_reset_email(request, url, reset_token):
    subject = 'Reset Password ['+settings.MY_HOST+']'
    message = 'Click on the link to reset your password.  '+url+str(reset_token.code)+'/'
    from_email = settings.FROM_EMAIL.get(settings.MY_HOST)
    send_mail(subject, message, from_email, [reset_token.user.email], fail_silently=False)

def send_password_set_email(request, url, reset_token):
    subject = 'Set Password ['+settings.MY_HOST+']'
    message = 'Click on the link to set your password.  '+url+str(reset_token.code)+'/'
    from_email = settings.FROM_EMAIL.get(settings.MY_HOST)
    send_mail(subject, message, from_email, [reset_token.user.email], fail_silently=False)

class UserViewset(LoggingMixin, ViewSet):
    def list(self, request):
        serialized = UserSerializer(request.user)
        return Response(serialized.data)

class LogoutViewset(LoggingMixin, APIView):
    def post(self, request):
        logout(request)
        return Response("successful logout")

class LoginViewset(LoggingMixin, APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        email = request.data.get('email', None)
        password = request.data.get('password', None)

        try:
            user = User.objects.get(username=email.lower())
        except User.DoesNotExist:
            return Response({
                'status': 'Unauthorized',
                'message': 'email/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

        user = authenticate(username=user.username, password=password)

        if user is not None:
            if user.is_active:
                login(request, user)
                serialized = UserSerializer(user)
                return Response(serialized.data)
            else:
                return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        else:
            return Response({
                'status': 'Unauthorized',
                'message': 'email/password combination invalid.'
            }, status=status.HTTP_401_UNAUTHORIZED)

import uuid, datetime
from models import AffiliateLink, ResetToken
class SignupViewset(LoggingMixin, APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        email = request.data.get('email', None)
        path = request.data.get('path', '')
        try:
            user = User.objects.get(username=email.lower())
            return Response({ 'message' : "account already exists for email address" }, status=status.HTTP_403_FORBIDDEN)
        except User.DoesNotExist:
            path = path.replace('/', '', 2)
            user = User.objects.create(email=email, username=email.lower())
            AffiliateLink.objects.create(user=user, url=path)
        try:
            reset_token = ResetToken.objects.get(user=user)
            reset_token.code = uuid.uuid4()
            reset_token.date = datetime.datetime.now()
            reset_token.save()
        except ResetToken.DoesNotExist:
            reset_token = ResetToken.objects.create(user=user, code=uuid.uuid4())
        send_password_set_email(request, settings.MY_HOST+"/reset/", reset_token)
        return Response("email sent (check spam folder)")

class ResetViewset(LoggingMixin, APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        email = request.data.get('email', None)
        url = request.data.get('url', None)

        if email == None or url == None:
            return Response({
                    'status': 'Unauthorized',
                    'message': 'Invalid formData.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        try:
            user = User.objects.get(username=email.lower())
        except User.DoesNotExist:
            return Response({
                    'status': 'Unauthorized',
                    'message': 'This account has been disabled.'
                }, status=status.HTTP_401_UNAUTHORIZED)

        try:
            reset_token = ResetToken.objects.get(user=user)
            reset_token.code = uuid.uuid4()
            reset_token.date = datetime.datetime.now()
            reset_token.save()
        except ResetToken.DoesNotExist:
            reset_token = ResetToken.objects.create(user=user, code=uuid.uuid4())
        send_reset_email(request, url, reset_token)
        return Response("email sent (check spam folder)")

class ChangePasswordViewset(LoggingMixin, APIView):
    permission_classes = (AllowAny,)
    def post(self, request):
        password = request.data.get('password', None)
        confirm_password = request.data.get('confirm_password', None)
        reset_token = request.data.get('reset_token', None)

        if password == None or confirm_password == None or reset_token == None or password != confirm_password:
            return Response({
                    'status': 'Unauthorized',
                    'message': 'Invalid input.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        try:
            token = ResetToken.objects.get(code=reset_token)
        except ResetToken.DoesNotExist:
            return Response({
                    'status': 'Unauthorized',
                    'message': 'Invalid token - use latest reset link.'
                }, status=status.HTTP_401_UNAUTHORIZED)
        user = token.user
        user.set_password(password)
        user.save()
        login(request, user)
        serialized = UserSerializer(user)
        return Response(serialized.data)
