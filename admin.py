# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.contrib import admin

# Register your models here.
from models import AffiliateLink, ResetToken

class AffiliateLinkAdmin(admin.ModelAdmin):
    pass
admin.site.register(AffiliateLink, AffiliateLinkAdmin)

class ResetTokenAdmin(admin.ModelAdmin):
    pass
admin.site.register(ResetToken, ResetTokenAdmin)
