# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

from django.conf import settings

class ResetToken(models.Model):
    user = models.OneToOneField(settings.AUTH_USER_MODEL)
    date = models.DateTimeField(auto_now_add=True, blank=True)
    code = models.CharField(max_length=256, unique=True)
    def __str__(self):
        return '%s' % (self.code)

class AffiliateLink(models.Model):
    url = models.CharField(max_length=256, blank=True, null=True)
    user = models.OneToOneField(settings.AUTH_USER_MODEL, related_name="affiliate_link")
    def __str__(self):
        return '%s %s' % (self.user, self.url)
