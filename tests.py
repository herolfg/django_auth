# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.urls import reverse
from rest_framework import status
from rest_framework.test import APIClient, APITestCase
from rest_framework.authtoken.models import Token

from models import ResetToken, AffiliateLink

from django.contrib.auth import get_user_model
from django.conf import settings

class DjangoAuthTests(APITestCase):
    def test_signup_workflow(self):
        client = APIClient()
        signup_url = "/"+settings.BASE_API+"authy/signup/"
        response = client.post(signup_url, { "email" : "gavin@herolfg.com" })
        self.assertEqual(response.status_code, status.HTTP_200_OK)
        self.assertEqual(get_user_model().objects.count(), 1)
        self.assertEqual(ResetToken.objects.count(), 1)
        self.assertEqual(AffiliateLink.objects.count(), 1)

        # test_change_password
        client = APIClient()
        reset_token = ResetToken.objects.all()[0]
        change_password_url = "/"+settings.BASE_API+"authy/change-password/"
        self.assertEqual(len(get_user_model().objects.get().password), 0)
        response = client.post(change_password_url, { "password" : "herolfg", "confirm_password" : "herolfg", "reset_token" : reset_token.code })
        self.assertNotEqual(len(get_user_model().objects.get().password), 0)
        self.assertEqual(response.status_code, status.HTTP_200_OK)
